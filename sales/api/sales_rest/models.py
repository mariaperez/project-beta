from django.db import models

# Create your models here.
# A Salesperson model containing first_name, last_name, employee_id fields.


class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.IntegerField()

    def __str__(self):
        return self.first_name


# A Customer model containing first_name, last_name, address,
#  and phone_number fields.

class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=20)

    def __str__(self):
        return self.first_name

# A Sale model containing automobile, salesperson, customer and price fields.
#  All fields except for price should be foreign key fields.


class Sale(models.Model):
    automobile = models.ForeignKey(
        'AutomobileVO', on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        'Salesperson', on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        'Customer', on_delete=models.CASCADE
    )
    price = models.IntegerField()

    def __str__(self):
        return f"Sale ID: {self.id} - Automobile: {self.automobile}"

# An AutomobileVO model containing vin and sold fields.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50)
    sold = models.BooleanField(default=False, null=True)

    def __str__(self):
        return self.vin


# class Technician(models.Model):
#     first_name = models.CharField(max_length=200)
#     last_name = models.CharField(max_length=200)
#     employee_id = models.SmallIntegerField()

# class Appointment(models.Model):
#     date_time = models.DateTimeField()
#     reason = models.CharField(max_length=200)
#     status = models.CharField(max_length=200)
#     vin = models.CharField(max_length=200)
#     customer = models.CharField(max_length=200)
#     technician = models.ForeignKey(
#         Technician,
#         related_name = "appointments",
#         on_delete = models.CASCADE,
#     )
