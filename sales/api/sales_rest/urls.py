# from django.urls import path
# from . import views

# urlpatterns = [
#     path('sales/', views.list_sales, name='list_sales'),
#     path('sales/<int:sale_id>/', views.delete_sale, name='delete_sale'),
#     path('customers/', views.list_customers, name='list_customers'),
#     path('customers/<str:phone_number>/',
#          views.delete_customer, name='delete_customer'),

#     path("salespeople/", views.api_list_salespeople, name="salespeoplelist"),
#     path("salespeople/<int:id>/", views.api_salesperson_details,
#          name="salespersondetails"),


#     #     path('salespeople/', views.list_salespeople, name='list_salespeople'),
#     #     path('salespeople/<int:salesperson_id>/',
#     #          views.delete_salesperson, name='delete_salesperson'),
# ]
from .views import api_list_salespeople, api_salesperson_details, api_list_customers, api_customer_details, api_list_sales, api_sale_details
from django.urls import path

urlpatterns = [
    path("salespeople/", api_list_salespeople, name="salespeoplelist"),
    path("salespeople/<int:id>/", api_salesperson_details,
         name="salespersondetails"),


    path("customers/", api_list_customers, name="customerlist"),
    path("customers/<int:id>/", api_customer_details, name="customerdetails"),


    path("sales/", api_list_sales, name="saleslist"),
    path("sales/<int:id>/", api_sale_details, name="saledetails"),

]
