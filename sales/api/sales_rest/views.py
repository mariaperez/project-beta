from django.http import JsonResponse
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
import traceback


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "first_name", "last_name", "phone_number", "address"]

    # def get_extra_data(self, o):
    #     phone_num = str(o.phone_number)
    #     return {"phone_number": f'({phone_num[0:3:1]}) {phone_num[3:6:1]}-{phone_num[6:11:1]}'}


class AutoEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "vin",]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["id", "automobile", "salesperson", "customer", "price",]
    encoders = {
        "automobile": AutoEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {"price": str(o.price)}


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Not viable input"},
                status=400,
            )


@require_http_methods(["DELETE"])
def api_salesperson_details(request, id):
    if request.method == "DELETE":
        count, _ = Salesperson.objects.filter(pk=id).delete()
        return JsonResponse({"deleted:": count > 0}, status=200 if count > 0 else 404)


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Not viable input"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer_details(request, id):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=id)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            customers = Customer.objects.get(id=id)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=400,
            )
        customers.delete()
        return JsonResponse(
            {"message": "Customer deleted"},
            status=200,
        )
    else:
        content = json.loads(request.body)
        try:
            if "id" in content:
                customer = Customer.objects.get(id=id)
                content["id"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


# @require_http_methods(["GET", "POST"])
# def api_list_sales(request):
#     if request.method == "GET":
#         sales = Sale.objects.all()
#         return JsonResponse(
#             {"sales": sales},
#             encoder=SaleEncoder,
#             safe=False,
#         )
#     else:
#         try:
#             content = json.loads(request.body)
#             if "automobile" in content:
#                 try:
#                     auto = AutomobileVO.objects.get(vin=content["automobile"])
#                     content["automobile"] = auto
#                 except AutomobileVO.DoesNotExist:
#                     return JsonResponse(
#                         {"message": "Automobile not found"},
#                         status=404,
#                     )
#             if "salesperson" in content:
#                 try:
#                     salesperson = Salesperson.objects.get(
#                         id=content["salesperson"])
#                     content["salesperson"] = salesperson

#                 except Customer.DoesNotExist:
#                     return JsonResponse(
#                         {"message": "Customer not found"},
#                         status=404,
#                     )
#             if "customer" in content:
#                 try:
#                     customer = Customer.objects.get(id=content["customer"])
#                     content["customer"] = customer

#                 except Salesperson.DoesNotExist:
#                     return JsonResponse(
#                         {"message": "Salesperson not found"},
#                         status=404,
#                     )

#             sale = Sale.objects.create(**content)
#             return JsonResponse(
#                 {"sale": sale},
#                 encoder=SaleEncoder,
#                 safe=False,
#             )
#         except Exception as e:
#             traceback.print_exc()  # Print the exception details
#             return JsonResponse(
#                 {"message": "Not viable input"},
#                 status=400,
#             )


# @require_http_methods(["GET", "PUT", "DELETE"])
# def api_sale_details(request, id):
#     if request.method == "GET":
#         try:
#             sales = Sale.objects.get(id=id)
#         except Sale.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Sale does not exist"},
#                 status=400,
#             )
#         return JsonResponse(
#             sales,
#             encoder=SaleEncoder,
#             safe=False,
#         )
#     elif request.method == "DELETE":
#         try:
#             sales = Sale.objects.get(id=id)
#         except Sale.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Sales does not exist"},
#                 status=400,
#             )
#         sales.delete()
#         return JsonResponse(
#             {"message": "Sale deleted"},
#             status=200,
#         )
#     else:
#         try:
#             content = json.loads(request.body)
#             if "automobile" in content:
#                 auto = AutomobileVO.objects.get(vin=content["automobile"])
#                 content["automobile"] = auto
#             if "salesperson" in content:
#                 salesperson = Salesperson.objects.get(
#                     id=content["salesperson"])
#                 content["salesperson"] = salesperson
#             if "customer" in content:
#                 customer = Customer.objects.get(id=content["customer"])
#                 content["customer"] = customer
#             Sale.objects.filter(id=id).update(**content)
#             sale = Sale.objects.get(id=id)
#             return JsonResponse(
#                 sale,
#                 encoder=SaleEncoder,
#                 safe=False,
#             )
#         except AutomobileVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Automobile not found"},
#                 status=404,
#             )
#         except Customer.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Customer not found"},
#                 status=404,
#             )
#         except Salesperson.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Salesperson not found"},
#                 status=404,
#             )
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        serialized_sales = []
        for sale in sales:
            serialized_sale = {
                "id": sale.id,
                "price": str(sale.price),
                "automobile": sale.automobile.vin,
                "salesperson": f"{sale.salesperson.first_name} {sale.salesperson.last_name}",
                "customer": f"{sale.customer.first_name} {sale.customer.last_name}",
            }
            serialized_sales.append(serialized_sale)
        return JsonResponse(
            {"sales": serialized_sales},
            encoder=SaleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            price = content["price"]
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            customer = Customer.objects.get(id=content["customer"])
            sale = Sale.objects.create(
                price=price,
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
            )

            automobile.import_sold = True
            automobile.save()

            serialized_sale = {
                "id": sale.id,
                "price": str(sale.price),
                "automobile": sale.automobile.vin,
                "salesperson": f"{sale.salesperson.first_name} {sale.salesperson.last_name}",
                "customer": f"{sale.customer.first_name} {sale.customer.last_name}",
            }

            return JsonResponse(
                serialized_sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except (
            AutomobileVO.DoesNotExist,
            Salesperson.DoesNotExist,
            Customer.DoesNotExist,
        ):
            return JsonResponse(
                {"error": "Could not create the sale"},
                status=404,
            )


@require_http_methods(["DELETE"])
def api_sale_details(request, id):
    if request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted:": count > 0}, status=200 if count > 0 else 404)
