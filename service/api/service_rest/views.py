import datetime

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import AppointmentEncoder, TechEncoder
from .models import Technician, Appointment
import json


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"technicians": techs},
            encoder=TechEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            tech = Technician.objects.create(**content)
            return JsonResponse(
                {"technican": tech},
                encoder=TechEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Incorrect input"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def technician_details(request, id):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(id=id)
            return JsonResponse(
                {"technician": tech},
                encoder=TechEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            tech = Technician.objects.get(id=id)
            tech.delete()
            return JsonResponse({"message": "Technician deleted"})
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=id).update(**content)
            tech = Technician.objects.get(id=id)
            return JsonResponse(
                {"technician": tech},
                encoder=TechEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=404,
            )
        except json.decoder.JSONDecodeError:
            return JsonResponse(
                {"message": "JSON decode error"},
                status=404,
            )

@require_http_methods(["GET", "POST"])
def list_appointments(request, status="created"):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.filter(status=status)
            return JsonResponse(
                {"appointment": appointments},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            if "technician" in content:
                try:
                    tech = Technician.objects.get(id=content["technician"])
                    content["technician"] = tech
                except Technician.DoesNotExist:
                    return JsonResponse(
                        {"message": "Technician does not exist"},
                        status=404
                    )
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except json.decoder.JSONDecodeError:
            return JsonResponse(
                {"message": "JSON decode error"},
                status=404,
            )
        except:
            return JsonResponse(
                {"message": "Incorrect input"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def appointment_details(request, id, status="created"):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse({"message": "Appointment deleted"})
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404,
            )
    else:
        try:
            appointment = Appointment.objects.get(id=id)
            try:
                content = json.loads(request.body)
                if "technician" in content:
                    try:
                        tech = Technician.objects.get(id=content["technician"])
                        content["technician"] = tech
                    except Technician.DoesNotExist:
                        return JsonResponse(
                            {"message": "Technician does not exist"},
                            status=404
                        )
                Appointment.objects.filter(id=id).update(**content)
                appointment = Appointment.objects.get(id=id)
            except json.decoder.JSONDecodeError:
                if status == "created":
                    return JsonResponse(
                        {"message": "JSON decode error"},
                        status=404,
                    )
            if status == "cancel":
                appointment.cancel()
            if status == "finish":
                appointment.finish()
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404,
            )
