# Generated by Django 4.0.3 on 2023-06-07 00:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_automobilevo_sold_appointment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointment',
            name='date_time',
        ),
    ]
