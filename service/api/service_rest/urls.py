from django.urls import path
from .views import (
    list_technicians,
    list_appointments,
    technician_details,
    appointment_details,
)


urlpatterns = [
    path("technicians/", list_technicians, name="list_techs"),
    path("technicians/<int:id>/", technician_details, name="tech_details"),
    path("appointments/<str:status>/", list_appointments, name="list_appointments_of_status"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", appointment_details, name="appointment_details"),
    path("appointments/<int:id>/<str:status>/", appointment_details, name="appointment_update"),
]
