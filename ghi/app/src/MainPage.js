function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <div>
        <img src="https://gray-wowt-prod.cdn.arcpublishing.com/resizer/a8-Qj-GWM6zTyddnu-5ufd24Iss=/800x600/smart/filters:quality(70)/cloudfront-us-east-1.images.arcpublishing.com/gray/S73SKA7XANLOLK5SCD5ABVRCCE.jpg" className="img"></img>
      </div>
    </div>
  );
}

export default MainPage;
