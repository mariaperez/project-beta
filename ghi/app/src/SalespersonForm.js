import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

function DelButton({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8090/api/salespeople/${id}/`;
        const editConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            await response.json();
            navigate('/salespeople/', { replace: true });
        }
    };

    if (id !== undefined) {
        return (
            <div>
                <button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>
                    Delete
                </button>
            </div>
        );
    }
    return <></>;
}

function SalespersonForm() {
    const navigate = useNavigate();
    const { id } = useParams();
    const [salesPersonData, setSalesPersonData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    const firstRender = async () => {
        if (id !== undefined) {
            const detailUrl = `http://localhost:8090/api/salespeople/${id}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editSalesPerson = await response.json();
                setSalesPersonData({
                    first_name: editSalesPerson.first_name,
                    last_name: editSalesPerson.last_name,
                    employee_id: editSalesPerson.employee_id,
                });
            }
        }
    };

    const handleInput = (event) => {
        setSalesPersonData({
            ...salesPersonData,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id !== undefined) {
            const detailUrl = `http://localhost:8090/api/salespeople/${id}/`;
            const editConfig = {
                method: 'put',
                body: JSON.stringify(salesPersonData),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                await response.json();
                navigate('/salespeople/', { replace: true });
            }
        } else {
            const salesPersonUrl = 'http://localhost:8090/api/salespeople/';
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(salesPersonData),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(salesPersonUrl, fetchConfig);
            if (response.ok) {
                await response.json();
                setSalesPersonData({
                    first_name: '',
                    last_name: '',
                    employee_id: '',
                });
            }
        }
    };

    useEffect(() => {
        firstRender();
    }, [id]);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <h1>Salesperson Information</h1>
                    <form onSubmit={handleSubmit} id="salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={salesPersonData.first_name} placeholder="" name="first_name" id="first_name" type="text" className="form-control" required />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={salesPersonData.last_name} placeholder="" name="last_name" id="last_name" type="text" className="form-control" required />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={salesPersonData.employee_id} placeholder="" name="employee_id" id="employee_id" type="number" className="form-control" required />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={id} />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalespersonForm;
