import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from 'react-router-dom';

function DelButton({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8100/api/models/${id}/`;
        const editConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            const delModel = await response.json();
            navigate("/models/", { replace: true });
        }
    }

    if (id !== undefined) {
        return (<div><button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>Delete</button></div>);
    }
    return (<></>);
}

function VehicleForm(props) {
    const navigate = useNavigate();
    const [{ id }, setId] = useState(useParams());
    const [manufacturers, setManufacturers] = useState([]);
    const [modelData, setModelData] = useState({
        name: "",
        picture_url: "",
        manufacturer_id: "",
    });

    const firstRender = async () => {
        if (id !== undefined) {
            const detailUrl = `http://localhost:8100/api/models/${id}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editModel = await response.json();
                setModelData({
                    name: editModel.name,
                    picture_url: editModel.picture_url,
                    manufacturer_id: editModel.manufacturer.id,
                });
            }
        }
    };

    const handleInput = (event) => {
        setModelData({
            ...modelData,
            [event.target.name]: event.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id !== undefined) {
            const detailUrl = `http://localhost:8100/api/models/${id}/`;
            const editConfig = {
                method: "put",
                body: JSON.stringify(modelData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                const editModel = await response.json();
                navigate("/models/", { replace: true });
            }
        } else {
            const modelUrl = "http://localhost:8100/api/models/"
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(modelData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(modelUrl, fetchConfig);
            if (response.ok) {
                const newModel = await response.json();
                setModelData({
                    name: "",
                    picture_url: "",
                    manufacturer_id: "",
                });
            }
        }
    };

    const fetchManufacturerData = async () => {
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerUrl);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => { firstRender(); }, [id]);
    useEffect(() => { fetchManufacturerData(); }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <form onSubmit={handleSubmit} id="vehicle-form">
                        <h1>Vehicle model</h1>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={modelData.name} placeholder="" name="name" id="name" type="text" className="form-control" required />
                            <label htmlFor="name">Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={modelData.picture_url} placeholder="" name="picture_url" id="picture_url" type="url" className="form-control" required />
                            <label htmlFor="picture_url">Model picture url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleInput} value={modelData.manufacturer_id} placeholder="" name="manufacturer_id" id="manufacturer_id" className="form-select" required>
                                <option value={""}>Select a manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={id} />
                    </form>
                </div>
            </div>
        </div>
    );
};

export default VehicleForm;
