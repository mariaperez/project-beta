
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="techdropdown" data-bs-toggle="dropdown" aria-expanded="false">Technicians</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="techdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/">Technicians List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/create/">Create Technician</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="manufacturerdropdown" data-bs-toggle="dropdown" aria-expanded="false">Manufacturers</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="manufacturerdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers/">Manufacturers List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers/create/">Create Manufacturer</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="modeldropdown" data-bs-toggle="dropdown" aria-expanded="false">Models</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="modeldropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/models/">Models List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/models/create/">Create Model</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="autodropdown" data-bs-toggle="dropdown" aria-expanded="false">Automobiles</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="autodropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/">Automobiles List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/create/">Create Automobile</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="customerdropdown" data-bs-toggle="dropdown" aria-expanded="false">Customers</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="customerdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers/">Customers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/customers/create/">Create Customer</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="salespersondropdown" data-bs-toggle="dropdown" aria-expanded="false">Salespeople</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="salespersondropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/">Salespeople</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/create/">Create Salesperson</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link" to="/salespeople/history/">Salesperson History</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="salesdropdown" data-bs-toggle="dropdown" aria-expanded="false">Sales</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="salesdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/">List Sales</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/sales/create/">Create Sale</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="Autodropdown" data-bs-toggle="dropdown" aria-expanded="false">Cars</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="autodropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/">Cars in Stock</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/create/">Add Car</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="Appointmentdropdown" data-bs-toggle="dropdown" aria-expanded="false">Service Appointments</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="appointmentdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/">Appointment List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/create/">Create Appointment</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/finished/">Finished Appointments</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/canceled/">Canceled Appointments</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
