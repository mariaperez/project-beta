import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from 'react-router-dom';

function DelButton ({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8100/api/automobiles/${id}/`;
        const editConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            const delAuto = await response.json();
            navigate("/automobiles/", { replace: true });
        }
    }

    if (id !== undefined) {
        return (<div><button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>Delete</button></div>);
    }
    return (<></>);
}

function AutomobileForm() {
    const navigate = useNavigate();
    const [{ vin }, setId] = useState(useParams());
    const [models, setModles] = useState([]);
    const [automobileData, setAutomobileData] = useState({
        color: "",
        year: "",
        vin: "",
        model_id: "",
    });

    const firstRender = async () => {
        if (vin !== undefined) {
            const detailUrl = `http://localhost:8100/api/automobiles/${vin}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editAuto = await response.json();
                setAutomobileData({
                    color:editAuto.color,
                    year:editAuto.year,
                    vin:editAuto.vin,
                    model_id:editAuto.model.id,
                });
            }
        }
    };

    const handleInput = (event) => {
        setAutomobileData({
            ...automobileData,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (vin !== undefined) {
            const detailUrl = `http://localhost:8100/api/automobiles/${vin}/`;
            const editConfig = {
                method: "put",
                body: JSON.stringify(automobileData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                const editAuto = await response.json();
                navigate("/automobiles/", { replace: true });
            }
        } else {
            const automobileUrl = "http://localhost:8100/api/automobiles/"
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(automobileData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(automobileUrl, fetchConfig);
            if (response.ok) {
                const newAutomobile = await response.json();
                setAutomobileData({
                    color: "",
                    year: "",
                    vin: "",
                    model_id: "",
                });
            }
        }
    };

    const fetchModelData = async () => {
        const modelUrl = "http://localhost:8100/api/models/";
        const response = await fetch(modelUrl);
        if (response.ok) {
            const data = await response.json();
            setModles(data.models);
        }
    };

    useEffect(() => {firstRender();}, [vin]);
    useEffect(() => {fetchModelData();}, []);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <h1>Automobile Information</h1>
                    <form onSubmit={handleSubmit} id="automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={automobileData.color} placeholder="" name="color" id="color" type="text" className="form-control" required />
                            <label htmlFor="color">Automobile color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={automobileData.year} placeholder="" name="year" id="year" type="text" className="form-control" required />
                            <label htmlFor="year">Automobile year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={automobileData.vin} placeholder="" name="vin" id="vin" type="text" className="form-control" required />
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleInput} value={automobileData.model_id} placeholder="" name="model_id" id="model_id" className="form-select" required>
                                <option value="">Select a model</option>
                                {models.map(model => {
                                    return <option key={model.id} value={model.id}>{model.manufacturer.name} {model.name}</option>
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={vin} />
                    </form>
                </div>
            </div>
        </div>
    );
};

export default AutomobileForm;