
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import VehicleForm from './VehicleForm';
import VehicleList from './VehicleList';
import CustomerList from './CustomerList';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import SaleList from './SaleList';
import SaleForm from './SaleForm';
import SalesPersonHistory from './SalesPersonHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="appointments/">
            <Route index element={<AppointmentList status="" />} />
            <Route path="finished/" element={<AppointmentList status="finished" />} />
            <Route path="canceled/" element={<AppointmentList status="canceled" />} />
            <Route path="complete/" element={<AppointmentList status="completed" />} />
            <Route path="create/" element={<AppointmentForm />} />
            <Route path="edit/:id/" element={<AppointmentForm />} />
          </Route>
          <Route path="manufacturers/">
            <Route index element={<ManufacturerList />} />
            <Route path="create/" element={<ManufacturerForm />} />
            <Route path="edit/:id/" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobileList />} />
            <Route path="create/" element={<AutomobileForm />} />
            <Route path="edit/:vin/" element={<AutomobileForm />} />
          </Route>
          <Route path="customers/">
            <Route index element={<CustomerList />} />
            <Route path="create/" element={<CustomerForm />} />
            <Route path="edit/:id/" element={<CustomerForm />} />
          </Route>
          <Route path="salespeople/">
            <Route index element={<SalespersonList />} />
            <Route path="create/" element={<SalespersonForm />} />
            <Route path="edit/:id/" element={<SalespersonForm />} />
            <Route path="history/" element={<SalesPersonHistory />} />
          </Route>
          <Route path="sales/">
            <Route index element={<SaleList />} />
            <Route path="create/" element={<SaleForm />} />
            <Route path="edit/:id/" element={<SaleForm />} />
          </Route>
          <Route path="models/">
            <Route index element={<VehicleList />} />
            <Route path="create/" element={<VehicleForm />} />
            <Route path="edit/:id/" element={<VehicleForm />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<TechnicianList />} />
            <Route path="create/" element={<TechnicianForm />} />
            <Route path="edit/:id/" element={<TechnicianForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
