import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

function VehicleList() {
    const [models, setModles] = useState([]);

    const fetchModelData = async () => {
        const modelUrl = "http://localhost:8100/api/models/";
        const response = await fetch(modelUrl);
        if (response.ok) {
            const data = await response.json();
            setModles(data.models);
        }
    };
    useEffect(() => { fetchModelData(); }, []);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th className="text-center">Models</th>
                        <th className="text-center">Pictures</th>
                        <th className="text-center">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td className="text-center">{model.manufacturer.name} {model.name}</td>
                                <td className="text-center"><img src={model.picture_url} className="img" /></td>
                                <td className="text-center">
                                    <Link to={`/models/edit/${model.id}/`} relative="path">Edit</Link>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default VehicleList;
