import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchManufacturerData = async () => {
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerUrl);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {fetchManufacturerData();}, []);
    
    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th className="text-center">Name</th>
                        <th className="text-center">Logo</th>
                        <th className="text-center">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {return (
                        <tr key={manufacturer.id}>
                            <td className="text-center">{manufacturer.name}</td>
                            <td className="text-center">
                                <img src={manufacturer.picture_url} style={{height: "10em"}}></img>
                            </td>
                            <td className="text-center">
                                <Link to={`/manufacturers/edit/${manufacturer.id}/`} relative="path">Edit</Link>
                            </td>
                        </tr>
                    );})}
                </tbody>
            </table>
        </div>
    );
};

export default ManufacturerList;