import React, { useState, useEffect } from "react";


function SaleList() {
    const [sales, setSales] = useState([]);

    const fetchSaleData = async () => {
        const saleUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(saleUrl);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    };


    useEffect(() => {
        fetchSaleData();
    }
        , []);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer Name</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale) => (
                        <tr key={sale.id}>
                            <td>{sale.salesperson}</td>
                            <td>{sale.customer}</td>
                            <td>{sale.automobile}</td>
                            <td>{sale.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default SaleList;
