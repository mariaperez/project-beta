import React, { useState, useEffect } from "react"
import { useParams, useNavigate } from "react-router-dom";

function DelButton ({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8100/api/manufacturers/${id}/`;
        const editConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            const delManufacturer = await response.json();
            navigate("/manufacturers/", { replace: true });
        }
    }

    if (id !== undefined) {
        return (<div><button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>Delete</button></div>);
    }
    return (<></>);
}

function ManufacturerForm() {
    const navigate = useNavigate();
    const [{ id }, setId] = useState(useParams());
    const [manufacturerData, setManufacturerData] = useState({
        name: "",
    });

    const firstRender = async () => {
        if (id !== undefined) {
            const detailUrl = `http://localhost:8100/api/manufacturers/${id}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editManufacturer = await response.json();
                setManufacturerData({
                    name: editManufacturer.name,
                });
            }
        }
    };

    const handleInput = (event) => {
        setManufacturerData({
            ...manufacturerData,
            [event.target.name]: event.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id !== undefined) {
            const detailUrl = `http://localhost:8100/api/manufacturers/${id}/`;
            const editConfig = {
                method: "put",
                body: JSON.stringify(manufacturerData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                const editManufacturer = await response.json();
                navigate("/manufacturers/", { replace: true });
            }
        } else {
            const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(manufacturerData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(manufacturerUrl, fetchConfig);
            if (response.ok) {
                const newManufacturer = await response.json();
                setManufacturerData({
                    name: "",
                });
            }
        }
    };

    useEffect(() => {firstRender();}, [id]);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <h1>Manufacturer Information</h1>
                    <form onSubmit={handleSubmit} id="manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={manufacturerData.name} placeholder="" name="name" id="name" type="text" className="form-control" required />
                            <label htmlFor="name">Manufacturer name</label>
                        </div>
                        <div className="form-floating mb-3">
                        </div>
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={id} />
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ManufacturerForm;

