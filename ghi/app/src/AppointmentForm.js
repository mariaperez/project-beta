import React , { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

function DelButton ({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8080/api/appointments/${id}/`;
        const editConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            const delAppointment = await response.json();
            navigate("/appointments/", { replace: true });
        }
    }

    if (id !== undefined) {
        return (<div><button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>Delete</button></div>);
    }
    return (<></>);
}

function StatusChange ({ id, handleInput, appointmentData }) {
    if (id !== undefined) {
        return (
            <div className="mb-3">
                <select onChange={handleInput} value={appointmentData.status} placeholder="" name="status" id="status" className="form-select">
                    <option value="created">Created</option>
                    <option value="finished">Finished</option>
                    <option value="canceled">Canceled</option>
                </select>
            </div>
    );}
    return (<></>);
}

function AppointmentForm() {
    const navigate = useNavigate();
    const [{ id }, setId] = useState(useParams());
    const [technicians, setTechnicians] = useState([]);
    const [appointmentData, setAppointmentData] = useState({
        vin: "",
        customer: "",
        date: "",
        reason: "",
        technician: "",
        status: "created",
    });

    const firstRender = async () => {
        if (id !== undefined) {
            const detailUrl = `http://localhost:8080/api/appointments/${id}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editAppointment = await response.json();
                setAppointmentData({
                    vin: editAppointment.appointment.vin,
                    customer: editAppointment.appointment.customer,
                    date: editAppointment.appointment.date_time,
                    reason: editAppointment.appointment.reason,
                    technician: editAppointment.appointment.technician.id,
                    status: editAppointment.appointment.status,
                });
            }
        }
    };

    const handleInput = (event) => {
        setAppointmentData({
            ...appointmentData,
            [event.target.name]: event.target.value,
        })
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id !== undefined) {
            const detailUrl = `http://localhost:8080/api/appointments/${id}/`;
            const editConfig = {
                method: "put",
                body: JSON.stringify(appointmentData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                const editModel = await response.json();
                navigate("/appointments/", { replace: true });
            }
        } else {
            const appointmentUrl = "http://localhost:8080/api/appointments/";
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(appointmentData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(appointmentUrl, fetchConfig);
            if (response.ok) {
                const newAppointment = await response.json();
                setAppointmentData({
                    vin: "",
                    customer: "",
                    date: "",
                    reason: "",
                    technician: "",
                    status: "created",
                });
            }
        }
    };

    const fetchTechData = async () => {
        const techUrl = "http://localhost:8080/api/technicians/";
        const response = await fetch(techUrl);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };
    
    useEffect(() => {firstRender();}, [id]);
    useEffect(() => {fetchTechData();}, []);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <h1>Appointment Information</h1>
                    <form onSubmit={handleSubmit} id="appointment-form">
                    <div className="form-floating mb-3">
                            <input onChange={handleInput} value={appointmentData.vin} placeholder="" name="vin" id="vin" type="text" className="form-control" required />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={appointmentData.customer} placeholder="" name="customer" id="customer" type="text" className="form-control" required />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={appointmentData.date} placeholder="" name="date" id="date" type="date" className="form-control" required />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleInput} value={appointmentData.technician} placeholder="" name="technician" id="technician" className="form-select" required>
                                <option value="">Select a technician</option>
                                {technicians.map(tech => {
                                    return <option key={tech.id} value={tech.id}>{tech.first_name} {tech.last_name}</option>
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={appointmentData.reason} placeholder="" name="reason" id="reason" type="text" className="form-control" required />
                            <label htmlFor="reason">Reason for appointment</label>
                        </div>
                        <StatusChange id={id} handleInput={handleInput} appointmentData={appointmentData} />
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={id} />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;