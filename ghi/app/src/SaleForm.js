import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

function DelButton({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8090/api/sales/${id}/`;
        const editConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            const delSale = await response.json();
            navigate("/sales/", { replace: true });
        }
    }

    if (id !== undefined) {
        return (<div><button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>Delete</button></div>);
    }
    return (<></>);
}

function SalesForm() {
    const navigate = useNavigate();
    const [{ id }, setId] = useState(useParams());
    const [autos, setAutos] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesData, setSalesData] = useState({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
    });

    const firstRender = async () => {
        if (id !== undefined) {
            const detailUrl = `http://localhost:8090/api/sales/${id}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editSale = await response.json();
                setSalesData({
                    automobile: editSale.automobile.vin,
                    salesperson: editSale.salesperson.id,
                    customer: editSale.customer.id,
                    price: editSale.price,
                });
            }
        }
    };

    const handleInput = (event) => {
        setSalesData({
            ...salesData,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id !== undefined) {
            const detailUrl = `http://localhost:8090/api/sales/${id}/`;
            const editConfig = {
                method: "put",
                body: JSON.stringify(salesData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                const editSale = await response.json();
                navigate("/sales/", { replace: true });
            }
        } else {
            const salesUrl = "http://localhost:8090/api/sales/";
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(salesData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(salesUrl, fetchConfig);
            if (response.ok) {
                const newSales = await response.json();
                const sellUrl = `http://localhost:8100/api/automobiles/${salesData.automobile}/sell/`;
                const sellConfig = {
                    method: "put",
                    headers: {
                        "Content-Type": "application/json",
                    }
                };
                const sellResponse = await fetch(sellUrl, sellConfig);
                setSalesData({
                    automobile: "",
                    salesperson: "",
                    customer: "",
                    price: "",
                });
            }
        }
    }

    const fetchSalespersonData = async () => {
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespersonUrl);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    };

    const fetchCustomerData = async () => {
        const customerUrl = "http://localhost:8090/api/customers/";
        const response = await fetch(customerUrl);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    };

    const fetchAutoData = async () => {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(autoUrl);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    };

    useEffect(() => { firstRender(); }, [id]);
    useEffect(() => { fetchCustomerData(); fetchAutoData(); fetchSalespersonData(); }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <h1>Sales Information</h1>
                    <form onSubmit={handleSubmit} id="sales-form">
                        <div className="mb-3">
                            <select key={salesData.automobile} onChange={handleInput} value={salesData.automobile} placeholder="" name="automobile" id="automobile" className="form-select" required>
                                <option key="" value="">Select a car</option>
                                {autos.map(auto => {
                                    return <option key={auto.vin} value={auto.vin}>{auto.model.manufacturer.name} {auto.model.name} VIN: {auto.vin}</option>
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select key={salesData.salesperson} onChange={handleInput} value={salesData.salesperson} placeholder="" name="salesperson" id="salesperson" className="form-select" required>
                                <option key="" value="">Select a salesperson</option>
                                {salespeople.map(salesperson => {
                                    return <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select key={salesData.customer} onChange={handleInput} value={salesData.customer} placeholder="" name="customer" id="customer" className="form-select" required>
                                <option key="" value="">Select a customer</option>
                                {customers.map(customer => {
                                    return <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={salesData.price} placeholder="" name="price" id="price" type="number" className="form-control" required />
                            <label htmlFor="customer">Price</label>
                        </div>
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={id} />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
