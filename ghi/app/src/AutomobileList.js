import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchAutoData = async () => {
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(automobileUrl);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    };
    useEffect(() => {fetchAutoData();}, []);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th className="text-center">Model</th>
                        <th className="text-center">Year</th>
                        <th className="text-center">VIN</th>
                        <th className="text-center">Color</th>
                        <th className="text-center">Picture</th>
                        <th className="text-center">Sold</th>
                        <th className="text-center">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(auto => {return (
                        <tr key={auto.id}>
                            <td className="text-center">{auto.model.manufacturer.name} {auto.model.name}</td>
                            <td className="text-center">{auto.year}</td>
                            <td className="text-center">{auto.vin}</td>
                            <td className="text-center">{auto.color}</td>
                            <td className="text-center"><img src={auto.model.picture_url} className="img" /></td>
                            <td className="text-center"><input type="checkbox" checked={auto.sold} readOnly /></td>
                            <td className="text-center">
                                <Link to={`/automobiles/edit/${auto.vin}/`} relative="path">Edit</Link>
                            </td>
                        </tr>
                    );})}
                </tbody>
            </table>
        </div>
    );
};

export default AutomobileList;
