import React , { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

function DelButton ({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8090/api/customers/${id}/`;
        const editConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            const delCustomer = await response.json();
            navigate("/customers/", { replace: true });
        }
    }

    if (id !== undefined) {
        return (<div><button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>Delete</button></div>);
    }
    return (<></>);
}

function CustomerForm() {
    const navigate = useNavigate();
    const [{ id }, setId] = useState(useParams());
    const [customerData, setCustomerData] = useState({
        first_name:"",
        last_name:"",
        phone_number:"",
        address:"",
    });

    const firstRender = async () => {
        if (id !== undefined) {
            const detailUrl = `http://localhost:8090/api/customers/${id}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editCustomer = await response.json();
                setCustomerData({
                    first_name:editCustomer.first_name,
                    last_name:editCustomer.last_name,
                    phone_number:editCustomer.phone_number,
                    address:editCustomer.address
                });
            }
        }
    };

    const handleInput = (event) => {
        setCustomerData({
            ...customerData,
            [event.target.name]: event.target.value,
        });
        if (isNaN(customerData.phone_number)) {
            const phoneNumber = Number(customerData.phone_number.split("").filter(num => !isNaN(num) && num !== " ").join(""));
            console.log(phoneNumber);
            setCustomerData({
                ...customerData,
                phone_number: phoneNumber,
            });
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id !== undefined) {
            const detailUrl = `http://localhost:8090/api/customers/${id}/`;
            const editConfig = {
                method: "put",
                body: JSON.stringify(customerData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                const editCustomer = await response.json();
                navigate("/customers/", {replace: true});
            }
        } else {
            const customerUrl = "http://localhost:8090/api/customers/";
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(customerData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(customerUrl, fetchConfig);
            if (response.ok) {
                const newCustomer = await response.json();
                setCustomerData({
                    first_name:"",
                    last_name:"",
                    phone_number:"",
                    address:"",
                });
            }
        }
    }

    useEffect(() => {firstRender();}, [id]);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <h1>Customer Information</h1>
                    <form onSubmit={handleSubmit} id="customer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={customerData.first_name} placeholder="" name="first_name" id="first_name" type="text" className="form-control" required />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={customerData.last_name} placeholder="" name="last_name" id="last_name" type="text" className="form-control" required />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={customerData.phone_number} placeholder="" name="phone_number" id="phone_number" type="text" className="form-control" required />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={customerData.address} placeholder="" name="address" id="address" type="text" className="form-control" required />
                            <label htmlFor="address">Address</label>
                        </div>
                        
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={id} />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
