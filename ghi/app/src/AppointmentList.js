import React , { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function  AppointmentList(props) {
    const{ status } = props
    const [appointments, setAppointments] = useState([])

    const fetchAppointmentsData = async () => {
        const resp = await fetch('http://localhost:8080/api/appointments/')

        if(resp.ok) {
            const data = await resp.json()
            console.log(data)
            setAppointments(data.appointment)
        }
        else {
            console.error("Response Invalid")
        }
    };

    const finish = async (event) => {
        event.preventDefault();
        const appointmentUrl = `http://localhost:8080/api/appointments/${event.target.value}/finish/`
        const fetchConfig = {
            method: "put",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const data = await response.json();
            fetchAppointmentsData();
        }
    };

    const cancel = async (event) => {
        event.preventDefault();
        const appointmentUrl = `http://localhost:8080/api/appointments/${event.target.value}/cancel/`
        const fetchConfig = {
            method: "put",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const data = await response.json();
            fetchAppointmentsData();
        }
    };


    const pastOrCurrent = (appointment) => {
        if (status === "") {
            return (<>
                <button className="btn-success" onClick={finish} value={appointment.id}>Finish</button>
                <div></div>
                <button className="btn-danger" onClick={cancel} value={appointment.id}>Cancel</button>
            </>);
        }
        else { return (<div>{appointment.status}</div>); }
    }

    useEffect(()=> {fetchAppointmentsData();}, [status])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Customer</th>
                    <th>Status</th>
                    <th>Reason</th>
                    <th>VIN</th>
                    <th>Technician</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return(
                        <tr key={appointment.id}> 
                        <td>{appointment.id}</td>
                        <td>{appointment.date_time}</td>
                        <td>{appointment.customer}</td>
                        <td>{appointment.status}</td>
                        <td>{appointment.reason}</td>
                        <td>{appointment.vin}</td>
                        <td>{appointment.technician.employee_id}</td>
                        <td className="text-center">{pastOrCurrent(appointment)}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}

export default AppointmentList;